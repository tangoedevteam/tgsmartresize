# tgSmartResize - iframe resizer

This will resize the content iframe for tgUI Web Applications

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/tgui/tgSmartResize/master/dist/tgSmartResize.min.js
[max]: https://raw.github.com/tgui/tgSmartResize/master/dist/tgSmartResize.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/tgSmartResize.min.js"></script>
<script>
$(window).load(function(){
  resizeMainContent();
});
$(window).smartresize(function(){
  resizeMainContent();
});

</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
